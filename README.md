# My Personal Site #

Just a simple site with html css (using bootstrap) and maybe a little javascript

### What is this repository for? ###

* This is my personal website
* [git Markdown tips](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Open up index.html in your favorite modern browser 
* I prefer Firefox due to historical personal preferences

### Contribution guidelines ###

* If for some reason you want to make a change feel free to contact me